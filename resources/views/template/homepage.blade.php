<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eormas</title>
    <link rel="shortcut icon" href="{{asset('assets/img/logo.ico')}}" type="image/x-icon">
    <link href="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <style type="text/css">
        body {
            font-family: 'Open Sans', sans-serif !important;
            background-color: #f4f4f4;
            opacity: 1;
            background: repeating-linear-gradient(-45deg, #e6e7ff, #e6e7ff 2.5px, #f4f4f4 2.5px, #f4f4f4 12.5px);
        }

        .timeline {
            border-left: 1px solid hsl(0, 0%, 90%);
            position: relative;
            list-style: none;
        }

        .timeline .timeline-item {
            position: relative;
        }

        .timeline .timeline-item:after {
            position: absolute;
            display: block;
            top: 0;
        }

        .timeline .timeline-item:after {
            background-color: hsl(0, 0%, 90%);
            left: -38px;
            border-radius: 50%;
            height: 11px;
            width: 11px;
            content: "";
        }

        .navbar .navbar-nav .nav-link:hover {
            color: rgb(197, 218, 255);
        }

        .navbar .navbar-nav .nav-link {
            padding: 0.6em;
            font-size: 1.2em;
            transition: all 0.5s;
            color: white;
        }

        .navbar-custom {
            background-color: rgb(62, 85, 148);
        }

        .navbar-customd {
            background-color: rgb(50, 68, 118);
            color: #ffffff;
        }

        .navbar .navbar-brand {
            padding: 0 0.6em;
            font-size: 1.5em;
            font-weight: bold;
        }

        @media only screen and (min-width: 992px) {
            .navbar .navbar-nav .nav-item .nav-link {
                padding: 0 0.5em;
            }

            .navbar .navbar-nav .nav-item:not(:last-child) .nav-link {
                border-right: 2px solid #ffffff;
            }
        }

    </style>
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-customd static-top">
        <div class="container">
            <a class="atf-site-branding atf-white-logo" href="/"><img src="assets/img/logo.png" alt="Logo"></a>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-custom static-top border-bottom border-5 border-primary">
        <div class="container">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ url('/') }}"><i
                                class="fas fa-fw fa-home"></i> Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('view-ormas') }}"><i class="fas fa-fw fa-table"></i> Data
                            Ormas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('chart-ormas') }}"><i class="fas fa-fw fa-chart-area"></i>
                            Grafik Ormas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('chart-ormas') }}"><i class="fas fa-fw fa-image"></i>
                            Dokumentasi</a>
                    </li>
                    <li class="nav-item custom">
                        <a class="nav-link " href="{{ url('login') }}"><i class="fas fa-fw fa-sign-in-alt"></i>Login </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
    <footer class="text-center text-lg-start mt-5 footer navbar-fixed-bottom"
        style="background-color: rgb(62, 85, 148);">
        <div class="container d-flex justify-content-center py-5">
            <button type="button" class="btn btn-primary btn-lg btn-floating mx-2" style="background-color: #54456b;">
                <i class="fab fa-facebook-f"></i>
            </button>
            <button type="button" class="btn btn-primary btn-lg btn-floating mx-2" style="background-color: #54456b;">
                <i class="fab fa-youtube"></i>
            </button>
            <button type="button" class="btn btn-primary btn-lg btn-floating mx-2" style="background-color: #54456b;">
                <i class="fab fa-instagram"></i>
            </button>
            <button type="button" class="btn btn-primary btn-lg btn-floating mx-2" style="background-color: #54456b;">
                <i class="fab fa-twitter"></i>
            </button>
        </div>

        <!-- Copyright -->
        <div class="text-center text-white p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2022 Copyright
        </div>
        <!-- Copyright -->
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo/datatables-demo.js') }}"></script>
</body>

</html>
